window.OptanonWrapper = function () { }

// If we haven't enabled OneTrust, we can close it,
// which should just close the banner and give consent.
// https://community.cookiepro.com/s/article/UUID-d8291f61-aa31-813a-ef16-3f6dec73d643?language=en_US
if (process.env.oneTrustDisabled) {
  window.OneTrust.Close()
}
