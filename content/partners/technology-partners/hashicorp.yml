---
  title: GitLab and HashiCorp
  description: Unlock work-faster workflows to build better applications with GitLab and HashiCorp, a joint solution for secure GitOps automation.
  components:
    - name: partners-call-to-action
      data:
        title: GitLab and HashiCorp
        inverted: true
        body:
          - text: Unlock work-faster workflows to build better applications with GitLab and HashiCorp, a joint solution for secure GitOps automation.
        image:
          image_url: https://about.gitlab.com/images/logos/hashicorp.svg
          alt: Hashicorp logo
    - name: copy-media
      data:
        block:
            - header: Uniting developers and operators with secure workflows
              text: |
                GitLab is the DevOps platform delivered as a single application for everyone on your pipeline. Integrate HashiCorp Vault and Terraform with GitLab to standardize secrets management and secure GitOps workflows.
              image:
                image_url: https://about.gitlab.com/images/topics/featured-partner-hashicorp-1.png
              link_href: https://about.gitlab.com/blog/2019/09/17/gitlab-hashicorp-terraform-vault-pt-1/
              link_text: Learn more
              secondary_link_href: https://about.gitlab.com/free-trial/
              secondary_link_text: Start your free trial
    - name: 'partners-feature-showcase'
      data:
        header: Streamline infrastructure and application delivery with GitLab and HashiCorp
        image_url: https://about.gitlab.com/images/topics/featured-partner-hashicorp-2.png
        text: GitLab shrinks cycle times from hours to minutes, helping enterprise customers embrace the cloud via automated workflows. GitLab’s built-in planning, monitoring, and reporting solutions integrate with Terraform and Vault so cross-functional teams can quickly codify infrastructure and define service dependencies within a secure environment.
        cards:
          - header: Iterate
            text: See progressive contributions. Version control and collaboration reduce rework so happier developers can expand product roadmaps instead of repairing old roads.
          - header: Automate
            text: Secure the left way. Automated DevSecOps workflows increase uptime by reducing security and compliance risks for cloud operations.
          - header: Innovate
            text: Create, impress, repeat. Increase market share and revenue when your product is on budget, on time, and always up.
    - name: 'benefits'
      data:
        header: Get started with GitLab and HashiCorp Joint Solutions
        full_background: true
        cards_per_row: 2
        text_align: left
        benefits:
          - icon: https://about.gitlab.com/images/icons/icon-6.svg
            title: Terraform Cloud + GitLab.com
            description: Configure GitLab as a Git provider and version control system (VCS) for Terraform Cloud to store plans and sentinel policies to trigger automation pipelines in the cloud.
          - icon: https://about.gitlab.com/images/icons/icon-5.svg
            title: GitLab Provider
            description: Use Terraform to manage resources on your GitLab instance like groups, projects, users, and more to improve productivity by eliminating an engineer’s dependence on provisioning requests.
          - icon: https://about.gitlab.com/images/icons/icon-4.svg
            title: Terraform EE + GitLab EE
            description: Provide flexible, template-driven modular workflows via GitLab CI/CD that evoke Terraform plans for Infrastructure as Code (IaC).
          - icon: https://about.gitlab.com/images/icons/icon-3.svg
            title: Vault
            description: Vault is a single security control plane for operations and infrastructure. Many organizations choose Vault to manage Audit Command Language (ACL), secrets, and other sensitive data. As a joint solution, GitLab and Vault provide a cross-functional alternative to error-prone, document-based collaboration methods. Vault is the leading solution for secrets management and one of GitLab’s most popular customer workflow integration requests for DevSecOps.
    - name: 'pull-quote'
      data: 
        quote: Now it's so easy to deploy something and roll it back if there's an issue. It's taken the stress and the fear out of deploying into production.
        source: DAVE BULLOCK, DIRECTOR OF ENGINEERING AT WAG!
        link_text: ''
        shadow: true
    - name: copy-resources
      data:
        title: Discover the benefits of GitLab and HashiCorp
        block:
          - video:
              title: 'HashiCorp Vault and GitLab integration: Why and How?'
              video_url: https://www.youtube.com/embed/VmQZwfgp3aA?enablesjsapi=1
              label: Gitlab and Hashicorp
            resources:
              webcast:
                header: Webcasts
                links:
                  - text: GitLab and HashiCorp - A holistic guide to GitOps and the Cloud Operating Model 
                    link: https://about.gitlab.com/webcast/gitlab-hashicorp-gitops/
              whitepaper:
                header: Whitepapers
                links:
                  - text: GitOps:The Future of Infrastructure Automation - A panel discussion with Weaveworks, HashiCorp, Red Hat, and GitLab 
                    link: https://about.gitlab.com/why/gitops-infrastructure-automation/
                  - text: Empowering Developers and Operators through GitLab and HashiCorp   
                    link: https://www.hashicorp.com/resources/empowering-developers-and-operators-through-gitlab-and-hashicorp/
              blog:
                header: Blogs
                links:
                  - text: 'GitLab and HashiCorp: Providing application and infrastructure delivery workflows '
                    link: https://about.gitlab.com/blog/2019/09/17/gitlab-hashicorp-terraform-vault-pt-1/
                  - text: How Wag! cut their release process from 40 minutes to just 6 
                    link: https://about.gitlab.com/blog/2019/01/16/wag-labs-blog-post/
