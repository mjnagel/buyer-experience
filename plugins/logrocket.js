import LogRocket from 'logrocket'

(function () {
  // Only initialize LogRocket in production - we technically run Nuxt generate in "production" mode for review apps,
  // so this will check if the script is running on a site with the `about.gitlab.com` domain. That should skip review apps appropriately.
  if (window.location.hostname.includes('about.gitlab.com')) {
    initializeLogRocket();
  }
})()

function initializeLogRocket() {
  LogRocket.init('9jygng/gitlab', {
    shouldCaptureIP: false,
    shouldDebugLog: false,
    dom: {
      inputSanitizer: true,
    }
  })
}
