# Slippers UI

The Core Marketing Site is meant to be built with [slippers-ui](https://gitlab.com/gitlab-com/marketing/inbound-marketing/slippers-ui), the GitLab Marketing design system. It's included in this project as a [Nuxt plugin](https://nuxtjs.org/docs/2.x/directory-structure/plugins). 

Since this repository uses the [TypeScript](https://www.typescriptlang.org/) Nuxt.js installation, and Slippers UI does *not* use TypeScript, Nuxt is missing type information from the Slippers UI package. 

By default, this causes an error. In the long term, Slippers UI should consider providing typing information as part of its distribution. In the short term, we can inform Nuxt that `slippers-ui` is a `module` by writing a [declaration file](https://www.typescriptlang.org/docs/handbook/declaration-files/introduction.html) about it in `slippers-ui.d.ts`.

## Fonts

We use [nuxt/google-fonts](https://google-fonts.nuxtjs.org/setup) to manage our font stacks.
